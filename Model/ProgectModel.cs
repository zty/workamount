﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ProgectModel
    {
        public int ID { get; set; }
        public string ProjectName { get; set; }//项目名
        public string CustomerName { get; set; }//客户名
        public string LeaderName { get; set; }//负责人
        public DateTime StartDate { get; set; }//开始时间
        public DateTime EndtDate { get; set; }//结束时间
        public DateTime CreateDate { get; set; }//创建时间
        public int IsPay { get; set; }//是否付费
        public int IsDelete { get; set; }//删除
    }
}
