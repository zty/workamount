﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class BasicDataModel
    {
        public int ID { get; set; }
        public string Name { get; set; }//名字
        public string Remark { get; set; }//备注
        public int IsType { get; set; }//类型
        public int IsDelete { get; set; }//删除
    }
}
