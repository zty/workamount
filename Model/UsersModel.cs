﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class UsersModel
    {
        public int ID { get; set; }
        public string LoginName { get; set; }//登陆名
        public string ChineseName { get; set; }//中文名
        public string EnglishName { get; set; }//英文名
        public string Email { get; set; }//邮箱
        public string Password { get; set; }//密码
        public string Phone { get; set; }//电话
        public string Technology { get; set; }//主要技术
        public int IsDelete { get; set; }//删除
    }
}
