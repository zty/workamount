﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;

namespace WorkAmount.Controllers
{
    public class UserController : BaseController
    {
        public ActionResult UserList()
        {
            return View();
        }

        /// <summary>
        /// 获取用户
        /// </summary>
        /// <param name="pageindex"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetUser(int pageindex, int pagesize)
        {
            var temp = db.t_Users.Where(n => n.IsDelete == 1)
                .OrderBy(n => n.ID)
                .Skip(pagesize * (pageindex - 1))
                .Take(pagesize).ToList();
            ViewData["total"] = db.t_Users.Where(n => n.IsDelete == 1).Count();
            ViewData["pageindex"] = pageindex;
            ViewData["pagesize"] = pagesize;

            int pageIndex = pageindex;//当前页
            int pageSize = pagesize;//显示条数

            return PartialView("UserPartial", temp);
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddUser(UsersModel model)
        {
            var loginName = db.t_Users.FirstOrDefault(n => n.LoginName.Equals(model.LoginName));

            if (loginName == null)
            {
                t_Users tbUser = new t_Users();
                tbUser.LoginName = model.LoginName.Trim();
                tbUser.ChineseName = model.ChineseName.Trim();
                tbUser.EnglishName = model.EnglishName.Trim();
                tbUser.Email = model.Email.Trim();
                tbUser.Phone = model.Phone.Trim();
                tbUser.Password = "123456";
                tbUser.IsDelete = 1;
                tbUser.Technology = model.Technology;
                db.t_Users.Add(tbUser);
                try
                {
                    db.SaveChanges();
                    return Json(new MessageData(true, "添加成功"));
                }
                catch (Exception)
                {
                    return Json(new MessageData(false, "添加失败"));
                }
            }
            else
            {
                return Json(new MessageData(true, "用户名已存在"));
            }

        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditUser(UsersModel model)
        {
            var useredit = db.t_Users.FirstOrDefault(n => n.ID == model.ID);

            useredit.LoginName = model.LoginName.Trim();
            useredit.ChineseName = model.ChineseName.Trim();
            useredit.EnglishName = model.EnglishName.Trim();
            useredit.Email = model.Email.Trim();
            useredit.Phone = model.Phone.Trim();
            useredit.Technology = model.Technology;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }
        }

        /// <summary>
        /// 删除用户信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DelUser(int id)
        {
            var user = db.t_Users.FirstOrDefault(n => n.ID == id);
            user.IsDelete = 0;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "删除成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "删除失败"));
            }
        }


    }
}