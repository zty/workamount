﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WorkAmount.Controllers
{
    public class BasicDataController : BaseController
    {
        public ActionResult DataType()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetData(int pageIndex,int pageSize)
        {
            var temp = db.t_BasicData.Where(n => n.IsDelete == 1).OrderBy(n=>n.ID)
                         .Skip(pageSize * (pageIndex - 1))
                         .Take(pageSize).ToList();
            ViewData["total"] = db.t_BasicData.Where(n => n.IsDelete == 1).Count();
            ViewData["pageindex"] = pageIndex;
            ViewData["pagesize"] = pageSize;

            return PartialView("BasicDataPartial", temp);
        }

        [HttpPost]
        public ActionResult AddData(BasicDataModel model)
        {
            t_BasicData newData = new t_BasicData();
            newData.Name = model.Name;
            newData.Remark = model.Remark;
            newData.IsType = model.IsType;
            newData.IsDelete = 1;

            db.t_BasicData.Add(newData);
            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "添加成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "添加失败"));
            }
        }

        [HttpPost]
        public ActionResult EditData(BasicDataModel model)
        {
            var editData = db.t_BasicData.FirstOrDefault(n => n.ID == model.ID);
            editData.Name = model.Name;
            editData.Remark = model.Remark;
            editData.IsType = model.IsType;;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "修改成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "修改失败"));
            }
        }

        [HttpPost]
        public ActionResult DelData(int id)
        {
            var editData = db.t_BasicData.FirstOrDefault(n => n.ID == id);
            editData.IsDelete = 0;

            try
            {
                db.SaveChanges();
                return Json(new MessageData(true, "删除成功"));
            }
            catch (Exception)
            {
                return Json(new MessageData(false, "删除失败"));
            }
        }
    }
}