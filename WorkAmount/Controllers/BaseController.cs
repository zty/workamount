﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;

namespace WorkAmount.Controllers
{
    public class BaseController : Controller
    {
        protected WorkAmountEntities db = new WorkAmountEntities();
        protected t_Users CurrentUser
        {
            get
            {
                if (Session["CurrentUser"] != null)
                {
                    return Session["CurrentUser"] as Model.t_Users;
                }
                return null;
            }
        }
    }
}