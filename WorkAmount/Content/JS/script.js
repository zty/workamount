﻿//ajax请求 兼容IE
jQuery.support.cors = true;//ie9以下兼容

//自定义分页
function customPaging(clickName,pageNumber, pageSize, dataTotal) {
    if (dataTotal == 0) {
        return false
    }
    else
    {
        var pageTotal = Math.ceil(dataTotal / pageSize);
        var page = page;
        var result = '<div class="page-info">';
        if (dataTotal != 0) {
            result += '当前显示' + ((pageNumber - 1) * pageSize + 1) + '-' + ((pageNumber * pageSize > dataTotal) ? dataTotal : pageNumber * pageSize) + '条&nbsp;&nbsp;'
            result += '共' + dataTotal + '条数据'
            result += '</div>';
            result += '<div class="page-number">';
        } else {
            result += '当前共' + dataTotal + '条数据'
            result += '</div>';
            result += '<div class="page-number">';
        }


        //上一页
        if (pageNumber == 1) {
            result += '<span>上一页</span>';
        }
        else {
            result += '<a href="javascript:void(0)" onclick="' + clickName + '(' + (pageNumber - 1) + ')">上一页</a>';
        }

        //显示数字
        if (pageTotal < 8) {//少于8页
            for (var i = 0; i < pageTotal; i++) {
                if (i + 1 == pageNumber) {
                    result += '<span class="curr">' + (i + 1) + '</span>';
                }
                else {
                    result += '<a href="javascript:void(0)" onclick="' + clickName + '(' + (i + 1) + ')">' + (i + 1) + '</a>';
                }
            }
        }
        else {
            if (pageNumber < 5) {//前5页
                for (var i = 0; i < 5; i++) {
                    if (i + 1 == pageNumber) {
                        result += '<span class="curr">' + (i + 1) + '</span>';
                    }
                    else {
                        result += '<a href="javascript:void(0)" onclick="' + clickName + '(' + (i + 1) + ')">' + (i + 1) + '</a>';
                    }
                }
                result += '<span>...</span>';
                result += '<a href="javascript:void(0)" onclick="' + clickName + '(' + pageTotal + ')">' + pageTotal + '</a>';
            }
            else if (pageNumber > pageTotal - 4) {//后5页
                result += '<a href="javascript:void(0)" onclick="' + clickName + '(1)">1</a>';
                result += '<span>...</span>';
                for (var i = pageTotal - 5; i < pageTotal; i++) {
                    if (i + 1 == pageNumber) {
                        result += '<span class="curr">' + (i + 1) + '</span>';
                    }
                    else {
                        result += '<a href="javascript:void(0)" onclick="' + clickName + '(' + (i + 1) + ')">' + (i + 1) + '</a>';
                    }
                }
            }
            else {//中间3页
                result += '<a href="javascript:void(0)" onclick="' + clickName + '(1)">1</a>';
                result += '<span>...</span>';
                result += '<a href="javascript:void(0)" onclick="' + clickName + '(' + (pageNumber - 1) + ')">' + (pageNumber - 1) + '</a>';
                result += '<span class="curr">' + pageNumber + '</span>';
                result += '<a href="javascript:void(0)" onclick="' + clickName + '(' + (pageNumber + 1) + ')">' + (pageNumber + 1) + '</a>';
                result += '<span>...</span>';
                result += '<a href="javascript:void(0)" onclick="' + clickName + '(' + pageTotal + ')">' + pageTotal + '</a>';
            }
        }

        //下一页
        if (pageNumber == pageTotal) {
            result += '<span>下一页</span>';
        }
        else {
            result += '<a href="javascript:void(0)" onclick="' + clickName + '(' + (pageNumber + 1) + ')">下一页</a>';
        }

        result += '</div>';

        return result;
    }
}

function alertModal(title, content) {
    $.confirm({
        title: title,
        content: content,
        buttons: {
            ok: {
                text: "确定",
                btnClass: 'btn-primary',
                keys: ['enter']
            }
        }
    })

}

function alertDel(title, content, Action, id) {
    $.confirm({
        title: title,
        content: content,
        buttons: {
            confirm: {
                btnClass: 'btn-danger',
                text: '确定',
                action: function () {
                    $.ajax({
                        cache: true,
                        type: "POST",
                        url: Action,
                        data: { "id": id },// 你的formid
                        async: false,
                        error: function (request) {
                            alert("Connection error");
                        },
                        success: function (data) {
                            alertModal('提示！', data.Message);
                            createTable(1);
                        }
                    });
                }
            },
            cancel: {
                text: '取消',
                action: function () {
                }
            }
        }
    });
}