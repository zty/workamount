﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WorkAmount.Startup))]
namespace WorkAmount
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
